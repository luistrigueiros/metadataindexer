/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.etl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import lombok.SneakyThrows;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;

import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar.trigueiros
 */
@Component
public class Unzipper {

    static Logger logger = LoggerFactory.getLogger(Unzipper.class);

    public void unzipEntry(File sourceArchive, String archiveEntry, File destination) {
        try {
            unzipEntry(sourceArchive, archiveEntry, new FileOutputStream(destination));
        } catch (FileNotFoundException ex) {
            logger.error("Error accessing Zip[{}] => [{}] {}", sourceArchive, archiveEntry, ex);
        }
    }

    @SneakyThrows
    public void unzipEntry(File sourceArchive, String archiveEntry, OutputStream outputStream) {
        Preconditions.checkArgument(sourceArchive.exists(), "The file %s does not exists", sourceArchive.getAbsolutePath());
        Preconditions.checkArgument(sourceArchive.canRead(), "Cann´t read file %s", sourceArchive.getAbsolutePath());
        ZipFile zipFile = new ZipFile(sourceArchive);
        logger.debug("Openned zip {}", sourceArchive.getAbsolutePath());
        logger.debug("Reading entry {}", archiveEntry);
        ZipArchiveEntry entry = zipFile.getEntry(archiveEntry);

        InputStream content = zipFile.getInputStream(entry);
        IOUtils.copy(content, outputStream);
        zipFile.close();
        IOUtils.closeQuietly(content);
    }

    @SneakyThrows
    public List<String> listFiles(File source) {
        List<String> entryList = Lists.newArrayList();
        ZipFile zipFile = new ZipFile(source);
        Enumeration<? extends ZipEntry> entries = zipFile.getEntries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (!entry.isDirectory()) {
                String name = entry.getName();
                entryList.add(name);
            }
        }
        IOUtils.closeQuietly(zipFile);
        return entryList;
    }

}
