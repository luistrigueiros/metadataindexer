/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.etl;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar.trigueiros
 */
@Component
public class EtlRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("timer://inputDirScanTimer?period=5second&fixedRate=false")
                .to("bean:dirScanner?method=scan").split(body())
                .to("seda:input");
        from("seda:input")
                .to("bean:archiveIndexer?method=indexArchive");

    }

}
