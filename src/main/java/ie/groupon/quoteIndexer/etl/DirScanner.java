/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.etl;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Ordering;
import com.google.common.io.Files;
import java.io.File;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author oscart
 */
public class DirScanner {

    private final File rootDir;

    public DirScanner(File rootDir) {
        this.rootDir = rootDir;
    }

    public Collection<File> scan() {
        FluentIterable<File> preOrderTraversal = Files.fileTreeTraverser().preOrderTraversal(rootDir);
        final Ordering<File> ordering = Ordering.from(new FileLastModifiedComparator());
        List<File> orderedFiles = ordering.reverse().sortedCopy(preOrderTraversal);
        return Collections2.filter(orderedFiles, new FileFilterPredicate());
    }

    private static class FileLastModifiedComparator implements Comparator<File> {

        @Override
        public int compare(File file1, File file2) {
            return (int) (file1.lastModified() - file2.lastModified());
        }
    }

    private static class FileFilterPredicate implements Predicate<File> {

        @Override
        public boolean apply(File t) {
            return t.isFile();
        }
    }

}
