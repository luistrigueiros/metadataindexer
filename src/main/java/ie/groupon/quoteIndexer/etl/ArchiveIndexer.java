/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.etl;

import com.google.common.hash.Hashing;
import ie.groupon.quoteIndexer.domain.ArchiveRepository;
import ie.groupon.quoteIndexer.domain.Archive;

import java.io.File;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscart
 */
@Component("archiveIndexer")
public class ArchiveIndexer {

    static Logger logger = LoggerFactory.getLogger(ArchiveIndexer.class);

    @Autowired
    private ArchiveRepository archiveRepository;

    @Autowired
    private Unzipper unzipper;

    private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @SneakyThrows
    public void indexArchive(File sourceArchive) {
        final String absolutePath = sourceArchive.getAbsolutePath();
        String id = idFor(absolutePath);
        boolean isAlreadyIndexed = archiveRepository.exists(id);
        if (isAlreadyIndexed) {
            logger.debug("The file[{}] is already indexed ", sourceArchive.getAbsoluteFile());
            return;
        }
        Archive ar = new Archive();
        ar.setId(id);
        ar.setPath(absolutePath);
        final String baseName = FilenameUtils.getBaseName(absolutePath);
        ar.setArchiveDate(formatter.parse(baseName));
        ar.setQuotes(unzipper.listFiles(sourceArchive));
        archiveRepository.save(ar);
        logger.debug("Indexed file[{}]",absolutePath, id);
    }

    private String idFor(String path) {
        return Hashing.md5().hashString(path, Charset.defaultCharset()).toString();
    }
}
