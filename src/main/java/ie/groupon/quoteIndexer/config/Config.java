/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.config;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.File;
import javax.annotation.PostConstruct;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscart
 */
@Data
@Component
public class Config {

    @Value("${inputDir}")
    private String inputDir;

    @PostConstruct
    public void initEtl() throws Exception {
        Preconditions.checkState(Strings.isNullOrEmpty(inputDir) == false, "Please check inputDir config property");
        File importFolder = new File(inputDir);
        Preconditions.checkNotNull(importFolder.canRead());
    }
}
