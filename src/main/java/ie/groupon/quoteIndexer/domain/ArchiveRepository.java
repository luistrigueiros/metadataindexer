/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.domain;

import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 *
 * @author oscar.trigueiros
 */
public interface ArchiveRepository extends ElasticsearchRepository<Archive, String>{
      List<Archive>  findByQuotesContaining(String quoteId);
}
