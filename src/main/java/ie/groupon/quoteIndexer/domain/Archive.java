/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.domain;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import static org.springframework.data.elasticsearch.annotations.FieldType.Date;

/**
 *
 * @author oscar.trigueiros
 */
@Document(type="archive",indexName = "quote")
@Data
public class Archive {
    @Id
    private String id;

    private String path;
    @Field(type = Date, format = DateFormat.basic_date_time)    
    private Date archiveDate;
    
    private List<String> quotes = Collections.emptyList();
}
