/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.web;

import com.wordnik.swagger.annotations.ApiOperation;
import ie.groupon.quoteIndexer.domain.ArchiveEntry;
import ie.groupon.quoteIndexer.domain.ArchiveRepository;
import ie.groupon.quoteIndexer.domain.Archive;
import ie.groupon.quoteIndexer.etl.Unzipper;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author oscar.trigueiros
 */
@RestController
public class MainController {

    @Autowired
    private Unzipper unzipper;

    @Autowired
    private ArchiveRepository archiveRepository;

    @RequestMapping(value = "archive/page", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a paged list of archives")
    Page<Archive> page(@RequestParam(value = "page", required = false, defaultValue = "0") int page, @RequestParam(value = "size", required = false, defaultValue = "10") int size) {
        return archiveRepository.findAll(new PageRequest(page, size));
    }

    @RequestMapping(value = "archive/byId/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns an archive by id")
    Archive archiveById(@PathVariable String id) {
        return archiveRepository.findOne(id);
    }

    @RequestMapping(value = "archive/byQuoteId/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Resturns a list of archives containing a give quote id")
    List<Archive> archiveByQuoteId(@PathVariable String id) {
        return archiveRepository.findByQuotesContaining(id);
    }

    @RequestMapping(value = "archive/count", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the count of archives indexed")
    long count() {
        return archiveRepository.count();
    }

    @RequestMapping(value="archive/entry", method = RequestMethod.POST)
    @ApiOperation(value = "Given the archive id and entry returns the content")
    public void retriveQuote(@RequestBody ArchiveEntry archiveEntry, HttpServletResponse response) throws IOException {
        Archive ar = archiveRepository.findOne(archiveEntry.getArchiveId());
        final OutputStream outputStream = response.getOutputStream();
        //setResponseHeaders(response, entry);
        unzipper.unzipEntry(new File(ar.getPath()), archiveEntry.getEntry(), outputStream);
        outputStream.flush();
        IOUtils.closeQuietly(outputStream);
    }

    private void setResponseHeaders(HttpServletResponse response, String entry) {
        response.setContentType("text/plain");
        response.setHeader("Content-Disposition",
                String.format("attachment;filename=%s", entry));

    }

}
