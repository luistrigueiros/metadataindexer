'use strict';

var quoteIndexerServices = angular.module('quoteIndexerServices', ['ngResource']);

quoteIndexerServices.factory('QuoteIndexer', ['$resource',
  function($resource){
    return $resource('archive/count', {}, {
      count: {method:'GET'}
    });
  }]);