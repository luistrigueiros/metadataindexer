/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.groupon.quoteIndexer.etl;

import ie.groupon.quoteIndexer.config.Config;

import java.io.File;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author oscart
 */
public class FileSortingTest {

    private Config config;

    @Before
    public void init() {
        config = new Config();
        config.setInputDir("C:/work/123.ie/breakdown/quoteIndexer/src/test/data");
    }

    @Test
    public void simpleTest() {
        File rootDir = new File(config.getInputDir());
        DirScanner or = new DirScanner(rootDir);
        for (File f : or.scan()) {
            if (f.isFile()) {
                System.out.println(f.getAbsolutePath());
            }
        }
    }


}
